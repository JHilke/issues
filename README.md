# Issues
This Unity application showcases three issues that should be avoided in virtual reality development.

1. IPD issue
  - The interpupillary distance is multiplied by 0.2 meters in the green area. This required us to introduce a multiplier to OVRCameraRig class which allows us to skew the distance (IPD) between LeftEyeAnchor and RightEyeAnchor.
2. Too fast movement of in-game objects in virtual reality. The animated cube moves from one location to another within one frame, causing visible ghosting.
3. Two objects with different shape and color are shown for both eyes separately. This is done by utilising multipass rendering and applying a shader to a sphere. The shader uses `unity_StereoEyeIndex` to discern one eye from another within the shader. This allows us to draw the sphere in different color for each eye and also apply vertex extrusion to the sphere for one eye and not the other.

## How to use?

1. Build & Run Issues.exe
2. Teleport using Right thumbstick (push it forwards, point to desired area and release to teleport)

## Authors
- Juuso Säärelä, juuso.srel@gmail.com
- Joonas Hilke, joonas.hilke@gmail.com
