using UnityEngine;


/// <summary>
/// This class enables IPD altering when entering the desired collider area.
/// </summary>
public class CameraTrigger : MonoBehaviour
{
    /// <summary>
    /// Contains the OVRCameraRig game object
    /// </summary>
    private GameObject CameraRig;

    /// <summary>
    /// Runs when the game is started
    /// </summary>
    void Start()
    {
        // Find OVRCameraRig from the hierarchy
        CameraRig = GameObject.Find("OVRCameraRig");
    }
 
    /// <summary>
    /// When collider with tag "MainCamera" enters collider, IPDMultiplier is multiplied by 0.2 meters
    /// </summary>
    /// <param name="other">Collider that triggers the event</param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "MainCamera")
        {
            Debug.Log("Enter");
            // Update the IPD multiplier value that we introduced to the SDK to skew the IPD distance
            CameraRig.GetComponent<OVRCameraRig>().IPDMultiplier = 0.2f;
        }
    }

    /// <summary>
    /// When collider with tag "MainCamera" exits collider, IPD multiplier gets restored to original value
    /// </summary>
    /// <param name="other">Collider that triggers the event</param>
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "MainCamera")
        {
            Debug.Log("Exit");
            // Update the IPD multiplier value that we introduced to the SDK to skew the IPD distance
            CameraRig.GetComponent<OVRCameraRig>().IPDMultiplier = 0.0f;
        }
    }
}
