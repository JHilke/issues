using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

/// <summary>
/// This class implements a way to black out the HMD view when the user's head is outside of the desired collider area.
/// </summary>
public class PostProcessEdit : MonoBehaviour
{
    /// <summary>
    /// Contains a reference to current post process quick volume.
    /// </summary>
    private PostProcessVolume m_Volume;

    /// <summary>
    /// Grading object that is used for drawing a black overlay when the users head is out of the desired area.
    /// </summary>
    private ColorGrading grading;
  
    /// <summary>
    /// Runs when game is started
    /// </summary>
    void Start() 
    {
        // Create an instance of ColorGrading
        grading = ScriptableObject.CreateInstance<ColorGrading>();
    }   

    /// <summary>
    /// Runs when another collider enters this collider
    /// </summary>
    /// <param name="other">Collider that triggered the event</param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "MainCamera")
        {
            Debug.Log("Entered collider");

            // Set Override to true to enable the effect
            grading.enabled.Override(true);
            // Set color filter color
            grading.colorFilter.Override(new Color(0f, 0f, 0f));

            // Applies color grading object to the quick volume
            m_Volume = PostProcessManager.instance.QuickVolume(gameObject.layer, 100f, grading);
        }
    }

    /// <summary>
    /// Runs when another collider enters this collider
    /// </summary>
    /// <param name="other">Collider that triggered the event</param>
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "MainCamera")
        {
            Debug.Log("Exited collider");
            // set Override to true to enable the effect
            grading.enabled.Override(true);
            // Set color filter color

            grading.colorFilter.Override(new Color(1f, 1f, 1f));

            // Applies color grading object to the quick volume
            m_Volume = PostProcessManager.instance.QuickVolume(gameObject.layer, 100f, grading);
        }
    }
    /// <summary>
    /// Clean up the post process volume when this object is destroyed
    /// </summary>
    void OnDestroy()
    {
        RuntimeUtilities.DestroyVolume(m_Volume, true, true);
    }
}
